package com.wuzi.base.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @description: minio配置类
 * @author wuyp 
 */
@Data
@Component
@ConfigurationProperties(prefix = "minio")
public class MinioPropertiesConfig {
    
    private String endpoint;

    private String accessKey;

    private String secretKey;

    private Boolean secure;

    private String bucketName;
}
