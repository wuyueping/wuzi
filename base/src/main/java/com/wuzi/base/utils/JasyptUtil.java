package com.wuzi.base.utils;

import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;

public class JasyptUtil {

    public static void main(String[] args){
        StandardPBEStringEncryptor standardPBEStringEncryptor =new StandardPBEStringEncryptor();
        /*配置文件中配置如下的算法*/
        standardPBEStringEncryptor.setAlgorithm("PBEWithMD5AndDES");
        /*配置文件中配置的password*/
        standardPBEStringEncryptor.setPassword("jaspyt_wuzi2024");
        //加密
        String jasyptPasswordEN =standardPBEStringEncryptor.encrypt("miniowuzi");
        String jasyptPasswordEN1 =standardPBEStringEncryptor.encrypt("minio3390");
        //解密
        String jasyptPasswordDE =standardPBEStringEncryptor.decrypt(jasyptPasswordEN);
        String jasyptPasswordDE1 =standardPBEStringEncryptor.decrypt(jasyptPasswordEN1);
        System.out.println("加密后密码："+jasyptPasswordEN);
        System.out.println("解密后密码："+jasyptPasswordDE);
        System.out.println("加密后密码："+jasyptPasswordEN1);
        System.out.println("解密后密码："+jasyptPasswordDE1);
    }
}
