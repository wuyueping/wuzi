package com.wuzi.base.controller;

import com.wuzi.base.utils.MinioUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@RequestMapping("/minio")
public class MinioController {
    
    @Autowired
    private MinioUtil minioUtil;

    @PostMapping("/upload")
    public List<String> upload(@RequestParam(name = "multipartFile") MultipartFile[] multipartFile) {
        return minioUtil.upload(multipartFile);
    }

    @GetMapping("/download")
    public ResponseEntity<byte[]> download(@RequestParam("fileName") String fileName) {
        return minioUtil.download(fileName);
    }
}
